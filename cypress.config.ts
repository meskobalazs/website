import { defineConfig } from 'cypress'

export default defineConfig({
  component: {
    devServer: {
      framework: 'vue',
      bundler: 'vite'
    }
  },

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'http://localhost:5173',
    supportFile: 'src/tests/cypress/support/e2e.{js,jsx,ts,tsx}',
    specPattern: 'src/tests/cypress/**/*.cy.{js,jsx,ts,tsx}',
    fixturesFolder: 'src/tests/cypress/fixtures',
    videosFolder: 'src/tests/cypress/videos',
    screenshotsFolder: 'src/tests/cypress/screenshot'
  }
})
