describe('In the home page', () => {
  it('click on the link in the header to go to the upload page', () => {
    cy.visit('/')
    cy.fixture('home').then((homeData) => {
      cy.contains(homeData.textLinkUpload).click()
      cy.url().should('include', '/partager-des-photos')
    })
  })
})

export {}
