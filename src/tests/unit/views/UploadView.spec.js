import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import UploadView from '../../../views/UploadView.vue'
import { createI18n } from 'vue-i18n'
import fr from '../../../locales/fr.json'
import { createRouter, createWebHistory } from 'vue-router'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})
const router = createRouter({
  history: createWebHistory(),
  routes: []
})
describe('Template', () => {
  it('should render the view with the button link', async () => {
    const wrapper = shallowMount(UploadView, {
      global: {
        plugins: [i18n, router],
        mocks: {
          $t: (msg) => msg,
          authConf: {
            enabled: true
          }
        }
      }
    })
    expect(wrapper.html()).contains('<link')
    expect(wrapper.html()).contains('path="')
    expect(wrapper.html()).contains('/auth/login')
    expect(wrapper.html()).contains('look="button"')
    expect(wrapper.html()).contains('type="external"')
  })
  it('should render the view without the button link', async () => {
    import.meta.env.VITE_API_URL = 'api-url/'
    const wrapper = shallowMount(UploadView, {
      global: {
        plugins: [i18n, router],
        mocks: {
          $t: (msg) => msg,
          authConf: {
            enabled: false
          }
        }
      }
    })
    expect(wrapper.html()).not.toContain('pages.upload.sub_title')
  })
})
