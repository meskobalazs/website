import { it, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import BetaText from '../../../components/BetaText.vue'
import { createI18n } from 'vue-i18n'
import fr from '../../../locales/fr.json'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

describe('Template', () => {
  it('should render the component with good wording keys', async () => {
    const wrapper = shallowMount(BetaText, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('general.header.title')
  })
})
