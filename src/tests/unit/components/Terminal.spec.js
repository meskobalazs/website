import { it, describe, expect, vi, beforeEach, afterEach } from 'vitest'
import { shallowMount, mount } from '@vue/test-utils'
import { createI18n } from 'vue-i18n'
import Terminal from '../../../components/Terminal.vue'
import Button from '../../../components/Button.vue'
import fr from '../../../locales/fr.json'
import * as copyToClipboardModule from '../../../utils/copyToClipboard'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

describe('Template', () => {
  it('should render the component with good wording keys', () => {
    const wrapper = shallowMount(Terminal, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('pages.upload.button_copy')
  })
  it('should render the view with the button', () => {
    const wrapper = shallowMount(Terminal, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('<button')
    expect(wrapper.html()).contains('icon="bi bi-clipboard-plus"')
    expect(wrapper.html()).contains('look="button--transparent"')
  })
})
describe('Props', () => {
  it('should have default props', () => {
    const wrapper = shallowMount(Terminal, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.vm.textUpload).toBe('')
    expect(wrapper.vm.textInstall).toBe('')
  })
  it('should have props filled', () => {
    const wrapper = shallowMount(Terminal, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      },
      props: {
        textUpload: 'upload',
        textInstall: 'install'
      }
    })
    expect(wrapper.vm.textUpload).toBe('upload')
    expect(wrapper.vm.textInstall).toBe('install')
  })
})
describe('Methods', () => {
  it('should have default clipboard img', () => {
    const wrapper = shallowMount(Terminal, {
      global: {
        plugins: [i18n],
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).contains('bi bi-clipboard-plus')
  })
  it('calls copyText function on button click', async () => {
    beforeEach(() => {
      vi.useFakeTimers()
    })
    afterEach(() => {
      vi.useRealTimers()
    })
    const wrapper = mount(Terminal, {
      global: {
        mocks: {
          $t: (msg) => msg
        },
        components: {
          Button
        }
      }
    })

    const updateClipboardMock = vi.spyOn(
      copyToClipboardModule,
      'updateClipboard'
    )
    updateClipboardMock.mockReturnValue(true)

    const spy = vi.spyOn(wrapper.vm, 'copyText')

    const buttonWrapper = wrapper.findComponent(Button)
    await buttonWrapper.vm.$emit('trigger')

    expect(spy).toHaveBeenCalled()
    expect(updateClipboardMock).toHaveBeenCalled()
    expect(wrapper.vm.uploadIsCopied).toBe(true)
  })
})
