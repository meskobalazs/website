import { vi, it, beforeEach, describe, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createI18n } from 'vue-i18n'
import { createRouter, createWebHistory } from 'vue-router'
import { useCookies } from 'vue3-cookies'
import fr from '../../../locales/fr.json'
import Header from '../../../components/Header.vue'
vi.mock('vue-router')
vi.mock('vue3-cookies', () => {
  const mockCookies = {
    get: vi.fn()
  }
  return {
    useCookies: () => ({
      cookies: mockCookies
    })
  }
})
const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})

const router = createRouter({
  history: createWebHistory(),
  routes: []
})

describe('Template', () => {
  describe('When the user is not logged', () => {
    it('should render the component with good wording keys', async () => {
      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = shallowMount(Header, {
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('general.header.contribute_text')
    })
    it('should render the component login link', async () => {
      const wrapper = shallowMount(Header, {
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('api/auth/login')
    })
  })
  describe('When the user is logged', () => {
    it('should render the component with good wording keys', async () => {
      vi.spyOn(useCookies().cookies, 'get').mockReturnValue('user_id=id')

      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = shallowMount(Header, {
        props: {
          authEnabled: true,
          userProfileUrl: 'profil'
        },
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('general.header.contribute_text')
      expect(wrapper.html()).contains('general.header.my_information_text')
      expect(wrapper.html()).contains('general.header.logout_text')
      expect(wrapper.html()).contains('general.header.sequences_text')
      expect(wrapper.html()).contains('general.header.my_settings_text')
    })
    it('should render the component with all links', async () => {
      vi.spyOn(useCookies().cookies, 'get').mockReturnValue('user_id=id')
      import.meta.env.VITE_API_URL = 'api-url/'
      const wrapper = shallowMount(Header, {
        props: {
          authEnabled: true,
          userProfileUrl: 'profil'
        },
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })
      expect(wrapper.html()).contains('auth/logout')
      expect(wrapper.html()).contains('path="/mes-informations"')
      expect(wrapper.html()).contains('path="/mes-parametres"')
      expect(wrapper.html()).contains('path="/mes-sequences"')
    })
  })
})

describe('Methods', () => {
  describe('toggleMenu', () => {
    let wrapper
    beforeEach(async () => {
      const VueRouter = await import('vue-router')
      VueRouter.useRoute.mockReturnValueOnce({
        path: 'my-path'
      })
    })
    it('Menu should be closed by default', () => {
      wrapper = shallowMount(Header, {
        global: {
          plugins: [i18n, router],
          mocks: {
            $t: (msg) => msg
          }
        }
      })

      expect(wrapper.vm.menuIsClosed).toBe(true)
    })
  })
})
