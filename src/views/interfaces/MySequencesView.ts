export interface LinkInterface {
  id: string
  href: string
  rel: string
  title: string
  type: string
  extent: { temporal: { interval: [Date[]] } }
  ['stats:items']: { count: number }
  ['geovisio:status']: string
}

export interface ExtentInterface {
  extent: { temporal: { interval: [Date[]] } }
  ['stats:items']: { count: number }
  title: string
}
