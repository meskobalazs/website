export interface OptionalViewerMapInterface {
  fetchOptions?: {
    credentials: string
  }
  picId?: string
}

export interface ViewerMapInterface extends OptionalViewerMapInterface {
  map: {
    startWide: boolean
    style?: object | string
    maxZoom: number
  }
}
