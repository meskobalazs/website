import type {
  UserSequenceInterface,
  ResponseUserPhotoInterface,
  ResponseUserPhotoLinksInterface
} from '@/views/interfaces/MySequenceView'

function imageStatus(
  imageStatus: string,
  userSequence: UserSequenceInterface
): string {
  if (userSequence.status === 'hidden') return userSequence.status
  return imageStatus
}

function scrollIntoSelected(
  id: string,
  userPhotos: ResponseUserPhotoInterface[]
): void {
  const itemPosition = userPhotos.map((el) => el.id).indexOf(id)
  const elementTarget = document.querySelector(`#photo${itemPosition}`)
  if (elementTarget) elementTarget.scrollIntoView()
}

function photoToDeleteOrPatchSelected(
  item: ResponseUserPhotoInterface,
  imagesToDelete: string[]
): boolean {
  return imagesToDelete.includes(item.id)
}

function spliceIntoChunks(arr: string[], chunkSize: number) {
  const res = []
  arr = ([] as string[]).concat(...arr)
  while (arr.length) {
    res.push(arr.splice(0, chunkSize))
  }
  return res
}

function formatPaginationItems(
  items: [ResponseUserPhotoLinksInterface]
): ResponseUserPhotoLinksInterface[] {
  const filterItems = items.filter(
    (el: ResponseUserPhotoLinksInterface) =>
      el.rel === 'first' ||
      el.rel === 'last' ||
      el.rel === 'next' ||
      el.rel === 'prev'
  )
  return filterItems.map((el: ResponseUserPhotoLinksInterface) => {
    if (el.rel === 'first') return { ...el, rel: 'double-left' }
    if (el.rel === 'last') return { ...el, rel: 'double-right' }
    if (el.rel === 'next') return { ...el, rel: 'right' }
    if (el.rel === 'prev') return { ...el, rel: 'left' }
    return el
  })
}

export {
  imageStatus,
  scrollIntoSelected,
  photoToDeleteOrPatchSelected,
  spliceIntoChunks,
  formatPaginationItems
}
