import { createRouter, createWebHistory } from 'vue-router'
import { useCookies } from 'vue3-cookies'
import type {
  RouteRecordRaw,
  NavigationGuardNext,
  RouteLocationNormalized
} from 'vue-router'
import axios from 'axios'
import { getAuthRoute } from '@/utils/auth'
import HomeView from '../views/HomeView.vue'
import MyInformationView from '../views/MyInformationView.vue'
import MySettingsView from '../views/MySettingsView.vue'
import MySequencesView from '../views/MySequencesView.vue'
import MySequenceView from '../views/MySequenceView.vue'
import UploadView from '../views/UploadView.vue'
const { cookies } = useCookies()
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/mes-informations',
    name: 'my-information',
    component: MyInformationView
  },
  {
    path: '/mes-parametres',
    name: 'my-settings',
    component: MySettingsView
  },
  {
    path: '/mes-sequences',
    name: 'my-sequences',
    component: MySequencesView
  },
  { path: '/sequence/:id', name: 'sequence', component: MySequenceView },
  {
    path: '/partager-des-photos',
    name: 'upload',
    component: UploadView
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeResolve(
  async (
    to: RouteLocationNormalized,
    from: RouteLocationNormalized,
    next: NavigationGuardNext
  ) => {
    const siteLoggedRoutes =
      to.name === 'my-settings' ||
      to.name === 'my-sequences' ||
      to.name === 'sequence'

    if (siteLoggedRoutes) {
      if (!isSiteLogged()) goToLoginPage(to.path)
      else return next()
    }
    if (to.name === 'my-information') {
      try {
        const keycloakLogout = await isKeycloakLogout()
        if (keycloakLogout.status >= 300 || !isSiteLogged()) {
          return goToLoginPage(to.path)
        } else return next()
      } catch (e) {
        return goToLoginPage(to.path)
      }
    }
    next()
  }
)

function isSiteLogged(): boolean {
  return !!cookies.get('user_id')
}

async function isKeycloakLogout(): Promise<{ status: number }> {
  const loginUrl = `/api/users/me`
  return await axios.get(loginUrl)
}

function goToLoginPage(path: string): void {
  window.location.replace(getAuthRoute('auth/login', path))
}

export default router
