export function img(name: string): string {
  return new URL(`../assets/images/${name}`, import.meta.url).toString()
}

export function getPicId(): string {
  return window.location.href.substring(
    window.location.href.indexOf('pic=') + 4,
    window.location.href.lastIndexOf('&')
  )
}
