import axios from 'axios'
import type {
  ViewerMapInterface,
  OptionalViewerMapInterface
} from '@/views/interfaces/common'
import GeoVisio from 'geovisio'

async function fetchMapAndViewer(params?: OptionalViewerMapInterface) {
  const tiles = import.meta.env.VITE_TILES
  let paramsGeovisio: ViewerMapInterface = {
    map: {
      startWide: true,
      maxZoom: 19
    }
  }
  if (tiles) {
    const style = tiles.includes('wxs.ign.fr') ? await getIgnTiles() : tiles
    paramsGeovisio = {
      map: {
        ...paramsGeovisio.map,
        style
      }
    }
  }
  if (params) {
    const { picId } = params
    const { fetchOptions } = params
    if (picId) paramsGeovisio = { ...paramsGeovisio, picId: picId }
    if (fetchOptions) {
      paramsGeovisio = {
        ...paramsGeovisio,
        fetchOptions: fetchOptions
      }
    }
  }
  return new GeoVisio(
    'viewer', // Div ID
    `${import.meta.env.VITE_API_URL}api/search`,
    paramsGeovisio
  )
}

async function getIgnTiles(): Promise<object> {
  const { data } = await axios.get(
    'https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/attenue.json'
  )
  data.sources.plan_ign.scheme = 'xyz'
  data.sources.plan_ign.attribution = 'Données cartographiques : © IGN'
  const objIndex = data.layers.findIndex(
    (el: any) => el.id === 'toponyme - parcellaire - adresse'
  )
  data.layers[objIndex].layout = {
    ...data.layers[objIndex].layout,
    'text-field': ['concat', ['get', 'numero'], ['get', 'indice_de_repetition']]
  }
  // Patch tms scheme to xyz to make it compatible for Maplibre GL JS / Mapbox GL JS
  // Patch num_repetition
  return data
}

export { fetchMapAndViewer }
