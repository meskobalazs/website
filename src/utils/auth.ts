function getAuthRoute(authRoute: string, nextRoute: string): string {
  const next = `${location.protocol}//${location.host}${nextRoute}`
  return `/api/${authRoute}?next_url=${encodeURIComponent(`${next}`)}`
}

export { getAuthRoute }
