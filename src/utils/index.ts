export default function title(title: string): string {
  const instanceName = import.meta.env.VITE_INSTANCE_NAME
  if (instanceName) return `${title} ${instanceName}`
  return title
}
