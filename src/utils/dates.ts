import moment from 'moment'

function formatDate(date: Date, formatType: string): string {
  const formatDate = moment(date)
  return formatDate.locale('fr').format(formatType)
}

function durationCalc(duration1: Date, duration2: Date, type: string): number {
  const duration = moment.duration(moment(duration1).diff(moment(duration2)))
  if (type == 'hours') return duration.hours()
  if (type == 'minutes') return duration.minutes()
  return duration.seconds()
}

export { formatDate, durationCalc }
