import axios from 'axios'
import { onMounted, ref } from 'vue'
import type { AuthConfigInterface } from './interfaces/Auth'

export default function authConfig() {
  const authConf = ref<AuthConfigInterface>({})

  async function getConfig(): Promise<object> {
    const { data } = await axios.get('api/configuration', {
      withCredentials: false
    })
    return data.auth
  }
  onMounted(async () => (authConf.value = await getConfig()))
  return { authConf }
}
