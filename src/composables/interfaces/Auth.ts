interface UserProfileInterface {
  url?: string
}
export interface AuthConfigInterface {
  enabled?: boolean
  user_profile?: UserProfileInterface
}
