# Changelog

All notable changes to this project will be documented in this file.

Before _0.1.0_ Changelog didn't exist.

## [0.1.0] -

### Added

- A new page `/mes-sequences` to access to a list of sequences for a logged user ([#14](https://gitlab.com/geovisio/website/-/issues/14)) :

  - the user can see all his sequences
  - the user can filter sequences
  - the user can enter to a specific sequence

- A new page `/sequence/:id` to access to a sequence of photos for a logged user ([#14](https://gitlab.com/geovisio/website/-/issues/14)) :
  - the user can see the sequence on the map and move on the map from photos to photos
  - the user can see information about the sequence
  - the user can see all the sequence's photos
  - the user can disable and delete one or many photo(s) of the sequence


### Changed

- Header have now a new entry `Mes photos` when the user is logged to access to the sequence list
- The router guard for logged pages has been changed to not call the api to check the token

### Fixed
