# Setup

## System requirements

**You need to have [Nodejs installed](https://nodejs.org/en/download)**
Node version : >=18.13.0

**You need to have [Npm installed](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)**

You can use npm or [yarn](https://yarnpkg.com/) as package manager

## Install

The website can be installed locally by retrieving this repository and installing dependencies:

```sh
# Retrieve source code
git clone https://gitlab.com/geovisio/website.git
cd website/

# Install dependencies
npm install
```

## Build for production

Before building, you need to define a bit of settings. At least, you have to create a `.env` file and edit its content.

```sh
cp env.example .env
```

More details about settings [can be found in docs here](./03_Settings.md).

Then, building for production can be done with these commands:

```sh
npm run build
PORT=3000 npm run start
```

The website is now available at [localhost:3000](http://localhost:3000).

## Next steps

You can check out [the available settings for your instance](./03_Settings.md).
